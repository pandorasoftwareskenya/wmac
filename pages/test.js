import {
  query,
  collection,
  orderBy,
  limit,
  getDocs,
  startAfter,
  onSnapshot,
  where,
} from "firebase/firestore";
import { useState, useEffect } from "react";
import { db } from "../firebase/clientApp";
import { useCollection } from "react-firebase-hooks/firestore";

export default function Test() {
  const [items, itemsLoading, itemsError] = useCollection(
    query(collection(db, "products"), orderBy("added", "desc"))
  );

  if (itemsLoading) return <div>Loading</div>;

  if (itemsError) return <div>Error</div>;

  return <div>{items.docs.length}</div>;
}
